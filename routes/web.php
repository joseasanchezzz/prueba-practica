<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/todo','VentaController@index')->name('home');
Route::get('/pe','VentaController@pe')->name('p');

Route::post('/pelic','VentaController@pelicula')->name('peli');

Route::get('/total','VentaController@total')->name('total');


