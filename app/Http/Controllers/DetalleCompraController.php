<?php

namespace App\Http\Controllers;

use App\Detalle_Compra;
use Illuminate\Http\Request;

class DetalleCompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Detalle_Compra  $detalle_Compra
     * @return \Illuminate\Http\Response
     */
    public function show(Detalle_Compra $detalle_Compra)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Detalle_Compra  $detalle_Compra
     * @return \Illuminate\Http\Response
     */
    public function edit(Detalle_Compra $detalle_Compra)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Detalle_Compra  $detalle_Compra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Detalle_Compra $detalle_Compra)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Detalle_Compra  $detalle_Compra
     * @return \Illuminate\Http\Response
     */
    public function destroy(Detalle_Compra $detalle_Compra)
    {
        //
    }
}
