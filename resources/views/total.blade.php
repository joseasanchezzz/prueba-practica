@extends('layout')
@section('title')
Listado de productos
@endsection
@section('content')
<h1 class="page-header">Listado de productos</h1>


    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Compra_ID</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Categoria</th>
            </tr>                            
        </thead>
        <tbody>


           
         
        </tbody>
    </table>



    <hr>
    <p>
        <a href="{{route('p')}}" class="btn btn-sm btn-primary">
            Ir al Peliculas
        </a>
        <a href="{{route('home')}}" class="btn btn-sm btn-primary">
            Ir al Todo
        </a>
       
        
    </p>
@endsection