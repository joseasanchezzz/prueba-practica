@extends('layout')
@section('title')
Listado de productos
@endsection
@section('content')
<h1 class="page-header">Inidique el genero de la pelicular</h1>

<div class="container">
    <form method="post"  action="{{route('peli')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group">
            <label for="exampleFormControlFile1">Indique el Genero</label>
             <input type="text" name="genero">
            <br><br>
            <input class="btn btn-primary" type="submit" value="Enviar" style="padding: 10px 20px;">
        </div>
    </form>
</div>
@endsection