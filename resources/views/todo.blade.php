@extends('layout')
@section('title')
Listado de productos
@endsection
@section('content')
<h1 class="page-header">Listado de productos</h1>


    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Compra_ID</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Categoria</th>
            </tr>                            
        </thead>
        <tbody>


            @foreach($p as $product)
            <tr>
                <td>{{ $product->id }}</td>
                        @foreach ($c as $k)
                        @if( $product->compra_id == $k->id)
                <td>{{ $k->producto }}</td>
                @endif
                @endforeach
                <td>{{ $product->nombre }}</td>
                <td>{{ $product->precio }}</td>
                @foreach ($ca as $l)
                        @if( $product->categoria_id == $l->id)
                <td>{{ $l->nombre }}</td>
                @endif
                @endforeach
               
            
            </tr> 
            @endforeach
        </tbody>
    </table>



    <hr>
    <p>
        <a href="{{route('p')}}" class="btn btn-sm btn-primary">
            Ir al Peliculas
        </a>
        <a href="{{route('total')}}" class="btn btn-sm btn-success">
            Ir al Total
        </a>
        
    </p>
    @endsection