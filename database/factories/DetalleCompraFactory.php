<?php

use Faker\Generator as Faker;

$factory->define(App\Detalle_Compra::class, function (Faker $faker) {
    return [
        'compra_id'=> rand(1,5),
		'nombre'=>$faker->text(rand(5,7)),
		'precio'=>rand(1,8),
		'categoria_id'=>rand(1,5)
    ];
});
