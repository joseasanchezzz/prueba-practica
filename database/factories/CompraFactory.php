<?php

use Faker\Generator as Faker;

$factory->define(App\Compra::class, function (Faker $faker) {
    return [
        'producto'=> $faker->text(rand(5,10)),
      'cantidad'=>rand(1,10)
    ];
});
