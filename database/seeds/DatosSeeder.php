<?php

use Illuminate\Database\Seeder;
use App\Categoria;
use App\Compra;
use App\Detalle_Compra;

class DatosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Categoria::class,10)->create();
        factory(Compra::class,10)->create();
        factory(Detalle_Compra::class,10)->create();
    }
}
