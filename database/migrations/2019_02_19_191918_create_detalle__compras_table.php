<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle__compras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->float('precio');
            $table->integer("categoria_id")->unsigned();
 $table->foreign('categoria_id')->references("id")->on("categorias")->onDelete('cascade')->onUpdate('cascade');
            $table->integer("compra_id")->unsigned();
     $table->foreign("compra_id")->references("id")->on("compras")->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle__compras');
    }
}
